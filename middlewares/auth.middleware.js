const isAuthenticated = (req, res, next) => {
    try {
        if (req.isAuthenticated()) {
            return next();
        }
    } catch (error) {
        return res.status(401).json(error.message);
    }
}


const isAdmin = (req, res, next) => {

    if (req.user.role === 'admin') {
        return next();
    } else {
        return res.send(403);
    }
}

module.exports = {
    isAuthenticated,
    isAdmin,
}