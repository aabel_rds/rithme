const mongoose = require('mongoose');
const DB_URL = require('../db').DB_URL;
const ConcertHall = require('../models/ConcertHall');

const concertHalls = [{
        name: "Sala Clamores",
        image: "https://pbs.twimg.com/profile_images/776755674877485056/JRhS94jD.jpg",
        location: "Calle de Alburquerque, 14",
        city: "Madrid",
    },
    {
        name: "Wizink Center",
        image: "https://yt3.ggpht.com/ytc/AAUvwniIr9pRSwKnP3HAPDJhpzTcLgf6qNAS9yucMsR90Q=s900-c-k-c0x00ffffff-no-rj",
        location: "Av. Felipe II, s/n",
        city: "Madrid"
    },
    {
        name: "Sala La Riviera",
        image: "https://verostec.com/images/logoriviera.jpg",
        location: "Paseo Bajo de la Virgen del Puerto, S/N",
        city: "Madrid",
    },
    {
        name: "Wanda Metropolitano",
        image: "https://pbs.twimg.com/profile_images/1275809648927531010/7_A1HrYb_400x400.jpg",
        location: "Av. de Luis Aragonés, 4,",
        city: "Madrid",
    },
    {
        name: "Caja Magica",
        image: "https://www.madrid-open.com/wp-content/uploads/2015/11/home_lacajamagica-1.jpg",
        location: "Cmo. de Perales, 23",
        city: "Madrid",
    },
    {
        name: "Sala Sol",
        image: "https://s3.eu-central-1.amazonaws.com/images.jacksonlive.es/upload/spots/1x1/1351587492277.jpg",
        location: "Calle de los Jardines, 3",
        city: "Madrid",
    },
]

mongoose.connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async() => {
        const allConcertHall = await ConcertHall.find();

        if (allConcertHall.length) {
            await ConcertHall.collection.drop();
        }
    })
    .catch((err) => {
        console.log(`Error deleting db data ${err}`);
    })
    .then(async() => {
        await ConcertHall.insertMany(concertHalls);
    })
    .catch((err) => {
        console.log(`Error adding data to our db ${err}`)
    })
    .finally(() => mongoose.disconnect());