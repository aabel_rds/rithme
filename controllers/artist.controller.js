const Artist = require('../models/Artist');

const indexGet = async (req, res) => {
    try {
        const artist = await Artist.find();

        return res.status(200).json(artist)
        
    } catch (error) {
        return res.status(500).json(error);
    }
};

module.exports = { indexGet }