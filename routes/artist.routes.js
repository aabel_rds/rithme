const express = require("express");
const artistController = require('../controllers/artist.controller');

const router = express.Router();

router.get("/", artistController.indexGet);

module.exports = router;