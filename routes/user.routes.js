const express = require('express');
const {isAuthenticated} = require('../middlewares/auth.middleware');
const usersController = require('../controllers/users.controller');
const { uploadToCloudinary, upload } = require('../middlewares/file.middleware');



const router = express.Router();

router.get('/tickets',[isAuthenticated],usersController.userGet);
router.get('/check-session', usersController.checkSession);
router.post('/register', usersController.registerPost);
router.post('/login' ,usersController.loginPost);
router.post('/logout',[isAuthenticated], usersController.logoutPost);
router.post('/recover-pass', usersController.recoverPass);
router.post('/edit', [upload.single('picture'), uploadToCloudinary], usersController.editPut);



module.exports = router;